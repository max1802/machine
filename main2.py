import Modules
import Algorithms
import numpy as np
import matplotlib.pyplot as plt
import cv2
import glob
import csv
import os
import pandas as pd

#настройки для подключения к Raspberry
host = '192.168.100.104'
user = 'pi'
secret = 'raspberry'
port = 22

#получение координат нажатия (первая строка не рабочая)
cord = np.zeros(shape=2, dtype=int)

#Координаты края платы
x0 = 0
y0 = 0

#ОБРАБОТКА СОБЫТИЙ
def getCoordinats(event,x,y,flags,param):

	if event == cv2.EVENT_LBUTTONDBLCLK:
		global cord
		cord = np.vstack([cord, [x,y]])

#КЛАСС RASPBERRY
def chessImage(num):

	chess = Modules.RaspberryPi(host, user, secret, port)
	for i in range(num):

		if i == 0:
			print ('Set the chess...')
		else:
			print ('Move the chess...')

		input()
		print ('OK')

		chess.commandSSH('raspistill -o chess.jpg')
		localpath = 'chess/'+'chess_' + str(i) + '.jpg'
		chess.copyData('chess.jpg', localpath)

def getImage(localpath, remotepath):

	image = Modules.RaspberryPi(host, user, secret, port)
	image.commandSSH('python bayer_demosaic.py')
	image.copyData(remotepath, localpath)

#КЛАСС ИЗОБРАЖЕНИЯ
def calibrationMatrix():
	# termination criteria
	criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

	# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
	objp = np.zeros((6*9,3), np.float32)
	objp[:,:2] = np.mgrid[0:9,0:6].T.reshape(-1,2)

	# Arrays to store object points and image points from all the images.
	objpoints = [] # 3d point in real world space
	imgpoints = [] # 2d points in image plane.

	images = glob.glob('chess/*.jpg')

	for fname in images:
	    img = cv2.imread(fname)
	    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

	    # Find the chess board corners
	    ret, corners = cv2.findChessboardCorners(gray, (9,6),None)

	    # If found, add object points, image points (after refining them)
	    if ret == True:
	        objpoints.append(objp)
	        corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
	        imgpoints.append(corners2)

	#cv2.destroyAllWindows()
	ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],None,None)

	np.save('chess/ret', ret)
	np.save('chess/mtx', mtx)
	np.save('chess/dist', dist)
	np.save('chess/rvecs', rvecs)
	np.save('chess/tvecs', tvecs)

def imageUndistortion(img):

	h,  w = img.shape[:2]
	newcameramtx, roi=cv2.getOptimalNewCameraMatrix(np.load('chess/mtx.npy'),np.load('chess/dist.npy'),(w,h),1,(w,h))
	mapx,mapy = cv2.initUndistortRectifyMap(np.load('chess/mtx.npy'),np.load('chess/dist.npy'),None,newcameramtx,(w,h),5)
	dst = cv2.remap(img,mapx,mapy,cv2.INTER_LINEAR)

	# crop the image
	x,y,w,h = roi
	dst = dst[y:y+h, x:x+w]

	return (dst)

def showImage(image, name = 'Image', size = .5, mouse = False):

	image = cv2.resize(image, (0,0), fx=size, fy=size)
	image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
	cv2.imshow(name, image)

	if mouse == True: 
		cv2.setMouseCallback(name, getCoordinats)

	cv2.waitKey()
	cv2.destroyAllWindows()

def changeImage(image):

	#image = cv2.convertScaleAbs(image, alpha = 2, beta = 30) #изменение яркости g(x)=αf(x)+β

	image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) #Greylevel = 0.299R + 0.587G + 0.114B
	#showImage(image, size=.4)
	#hist(image)
	clahe = cv2.createCLAHE(clipLimit=8.0, tileGridSize=(8,8))
	image = clahe.apply(image)
	
	image = cv2.medianBlur(image, 3)
	image = cv2.GaussianBlur(image,(3,3),0)
	#ret, image = cv2.threshold(image, 150, 150, cv2.THRESH_TRUNC)

	#hist(image)
	#showImage(image, size=.4)
	ret, image = cv2.threshold(image, 140, 255, 0)
	#showImage(image, size=.4)
	return(image)

def rotationImage(image):

    (h, w) = image.shape[:2]
    center = (int(w / 2), int(h / 2))
    rotation_matrix = cv2.getRotationMatrix2D(center, -.6, 1)
    #rotation_matrix = cv2.getRotationMatrix2D(center, 0, 1)
    image = cv2.warpAffine(image, rotation_matrix, (w, h))

    return(image)

def getComponents(image, rapperpoints, rapperpointsimg, scalecoef, scalecoefX, scalecoefY, path):

	global x0
	global y0

	sizeSMD = [] #Создаем словарь для размеров SMD корпусов
	componentsSMD = [] #Создаем словарь для компонентов проекта

	#Открываем csv файл и копируем значения в виде словаря каждую строку таблицы
	with open('base/sizeSMD.csv') as File:
		reader = csv.DictReader(File, delimiter=';')
		for row in reader:
			sizeSMD.append(row)

	#Ищем в массиве нужный словарь по паре значений Ключ-Значение 
	#print(next(item for item in sizeSMD if item['Body'] == '805'))

	#Открываем csv файл с компонентами проекта
	with open('projects/PK8-3_v8/componentsSMD.csv') as File:
		reader = csv.DictReader(File, delimiter=';')
		for row in reader:
			componentsSMD.append(row)

	#Рассчет крайних точек компонента (10 это поправочный, надо бы удалить)
	x0 = (rapperpointsimg['x1']+(rapperpoints['x1'])*scalecoefX) 
	y0 = (rapperpointsimg['y1']-(rapperpoints['y1'])*scalecoefY) 

	#Цикл для получения изображений компонентов
	for i in range(len(componentsSMD)):
		if componentsSMD[i]['Rotate'] == '0' or componentsSMD[i]['Rotate'] == '180':
			bodyX = float((next(item for item in sizeSMD if item['Body'] == componentsSMD[i]['Type']))['Width'])
			bodyY = float((next(item for item in sizeSMD if item['Body'] == componentsSMD[i]['Type']))['Height'])
		else:
			bodyX = float((next(item for item in sizeSMD if item['Body'] == componentsSMD[i]['Type']))['Height'])
			bodyY = float((next(item for item in sizeSMD if item['Body'] == componentsSMD[i]['Type']))['Width'])			

		x1 = float(x0) - float(componentsSMD[i]['X'])*scalecoefX - scalecoefX*bodyX
		x2 = float(x0) - float(componentsSMD[i]['X'])*scalecoefX + scalecoefX*bodyX
		y1 = float(y0) + float(componentsSMD[i]['Y'])*scalecoefY - scalecoefY*bodyY
		y2 = float(y0) + float(componentsSMD[i]['Y'])*scalecoefY + scalecoefY*bodyY		
		np.save(path+componentsSMD[i]['RefDes'], image[int(y1):int(y2), int(x1):int(x2)])
		#np.save('data/'+str(i), image[int(y1):int(y2), int(x1):int(x2)])

	return(componentsSMD)

def getCoordinates(image,path):

	showImage(image, size = imgcoef, mouse = True) #ВЫВОД ИЗОБРАЖЕНИЯ ДЛЯ ПОЛУЧЕНИЯ КООРДИНАТ

	#ПОЛУЧЕНИЕ КООРДИНАТ ЧЕРЕЗ ЦЕНТР МАССЫ
	rapperpointsimg = realPoints(image, rapperpoints)

	scalecoef = rapperpointsimg['dist']/rapperpoints['dist']
	scalecoefX = (rapperpointsimg['x1']-rapperpointsimg['x2'])/(rapperpoints['x2']-rapperpoints['x1'])
	scalecoefY = (rapperpointsimg['y2']-rapperpointsimg['y1'])/(rapperpoints['y2']-rapperpoints['y1'])

	#print(rapperpointsimg['x2']-rapperpointsimg['x1'])
	#print (rapperpoints['x2']-rapperpoints['x1'])
	a = np.array([rapperpointsimg ,scalecoef, scalecoefX, scalecoefY])
	np.save(path, a)

	#np.save('projects/PK8-3_v8/template/image/paramsrappoints.npy', a)
	#np.save('projects/PK8-3_v8/board_1/image/paramsrappoints.npy', a)
	#np.save('projects/PK8-3_v8/board_2/image/paramsrappoints.npy', a)
	#np.save('projects/PK8-3_v8/board_3/image/paramsrappoints.npy', a)

	#ОТОБРАЖЕНИЕ РЕПЕРНЫХ ТОЧЕК
	for i in range(1,len(cord)):
		img = cv2.circle(template, (rapperpointsimg['x'+str(i)],rapperpointsimg['y'+str(i)]), 5, (0,0,255), -1)
		img = cv2.circle(template, (220,260), 5, (0,0,255), -1)
		img = cv2.circle(template, (1800,260), 5, (0,0,255), -1)
		img = cv2.circle(template, (220,1260), 5, (0,0,255), -1)
		img = cv2.circle(template, (1800,1260), 5, (0,0,255), -1)

	showImage(img) #ПОКАЗАТЬ ИЗОБРАЖЕНИЕ

#ВСПОМОГАТЕЛЬНЫЕ ФУНКЦИИ:
def hist(image_gaussian):
	histr = cv2.calcHist([image_gaussian],[0],None,[256],[0,256])
	plt.plot(histr)
	plt.show()

def histColor(girl):

	color = ("b", "g", "r")
	for i, color in enumerate(color):
		hist = cv2.calcHist([girl], [i], None, [256], [0, 256])
		plt.title("girl")
		plt.xlabel("Bins")
		plt.ylabel("num of perlex")
		plt.plot(hist, color = color)
		plt.xlim([0, 260])
	plt.show()
	cv2.waitKey(0)
	cv2.destroyAllWindows()

def histResults(dict_template, dict_testimage, component_type, algorithm_type):

	recognized = 0
	false_positive = 0
	false_negative = 0
	hist = []
	hist2 = []

	for key, value in dict_template.items():

		#print (key)
		#hist.append(dict_testimage[key][0])
		#if key[:1] == component_type:
		if key[:(len(component_type))] == component_type:
			if dict_template[key] == dict_testimage[key][1]:
				recognized+=1
				
			elif dict_template[key] == 'bad' and dict_testimage[key][1] == 'good':
				false_positive+=1 
				print ('false_positive ',key)
			elif dict_template[key] == 'good' and dict_testimage[key][1] == 'bad':
				false_negative+=1
				print ('false_negative ',key)

			if dict_template[key] == 'good': hist.append(dict_testimage[key][0])
			else: hist2.append(dict_testimage[key][0])
	
	#print (hist)	 
	value = (recognized, false_positive, false_negative) 
	position = np.arange(3)
	fig, ax = plt.subplots()
	ax.bar(position, value)
	ax.set_title(algorithm_type + '\n' + component_type)
	#  Устанавливаем позиции тиков:
	ax.set_xticks(position)

	#  Устанавливаем подписи тиков
	ax.set_xticklabels(['Распознаные '+str(recognized),
						'Ложноположит. '+str(false_positive), 
						'Ложноотрицат. '+str(false_negative)])
	fig.set_figwidth(8)
	fig.set_figheight(6)
	plt.show()

	plt.boxplot([hist, hist2])
	plt.show()

def histResults2(dict_template, dict_testimage, component_type, algorithm_type):

	recognized = 0
	false_positive = 0

	false_positive_n = []
	false_negative_n = []

	false_negative = 0
	bad_components = 0
	hist0 = []
	hist = []
	hist2 = []

	for key, value in dict_template.items():

		#print (key)
		#hist.append(dict_testimage[key][0])
		if dict_template[key][0] == component_type:
		#if key[:(len(component_type))] == component_type:
			
			if dict_template[key][1] == dict_testimage[key][1][1]:
				recognized+=1
				
			elif dict_template[key][1] == 'bad' and dict_testimage[key][1][1] == 'good':
				false_positive+=1 
				print ('false_positive ', key, dict_testimage[key])
				false_positive_n.append(key)
				#print (len(false_positive_n))
				
			elif dict_template[key][1] == 'good' and dict_testimage[key][1][1] == 'bad':
				false_negative+=1
				print ('false_negative ', key, dict_testimage[key])
				false_negative_n.append(key)
				

			if dict_template[key][1] == 'good': hist.append(dict_testimage[key][1][0])
			else: hist2.append(dict_testimage[key][1][0])

			#Количество испорченых компонентов
			if dict_template[key][1] == 'bad':
				bad_components+=1

	
	#РАСЧЕТ ЭФФЕКТИВНОСТИ АЛГОРИТМА
	x_mean = bad_components
	x_true = bad_components-(len(false_positive_n)+len(false_negative_n))
	#print('%',((x_mean)-(x_true))/x_mean)
	 
	value = (recognized, len(false_positive_n), len(false_negative_n)) 
	position = np.arange(3)

	d = {'Good':pd.Series(hist), 'Bad':pd.Series(hist2)}
	df = pd.DataFrame(d)
	print (df.describe())

	#ЯЩИКИ С УСАМИ
	fig, ax = plt.subplots()
	#print('Без деффектов',len(hist), len(hist2))
	ax.set_xticklabels(['Good '+ str(len(hist)), 'Bad '+str(len(hist2))])
	fig.set_figwidth(4)
	fig.set_figheight(3)
	plt.boxplot([hist, hist2])
	plt.show()

	#ГИСТОГРАММА
	fig, ax = plt.subplots()
	fig.set_figwidth(4)
	fig.set_figheight(3)
	plt.hist([hist,hist2])
	plt.show()

	#ГИСТОГРАММА С РЕЗУЛЬТАТАМИ
	fig, ax = plt.subplots()
	ax.bar(position, value)
	ax.set_title('Алгоритм: '+algorithm_type + '\n' + 'Тип компонента: '+component_type)
	ax.set_xticks(position)
	ax.set_xticklabels(['Распознаные '+str(recognized),
						'Ложноположит. '+str(false_positive), 
						'Ложноотрицат. '+str(false_negative)])
	fig.set_figwidth(6)
	fig.set_figheight(4)
	plt.show()



	return(false_positive_n, false_negative_n)

def resultPerfect(dict_template, board_deffect):
	#Обозначение компонентов, которые испорчены

	for i in range(len(board_deffect)):
		dict_template[board_deffect[i]] = 'bad'

	return (dict_template)

def resultPerfect2(dict_template, board_deffect):
	#Обозначение компонентов, которые испорчены

	for i in range(len(board_deffect)):
		dict_template[board_deffect[i]][1] = 'bad'
	#print (dict_template)
	return (dict_template)

#ТЕСТОВЫЕ ФУНКЦИИ:
def realPoints(image, rapperpoints ,size = .5):

	newcontours = []
	m = []
	realcord = np.zeros(shape=2, dtype=int)
	coef = int(pow(size, -1))

	for i in range(1, len(cord)):
		contours, hierarchy = cv2.findContours(image[((cord[i][1]-5)*coef):((cord[i][1]+5)*coef),
			((cord[i][0]-5)*coef):((cord[i][0]+5)*coef)], 
			cv2.RETR_EXTERNAL, 
			cv2.CHAIN_APPROX_SIMPLE)

		newcontours.append(contours[0]+[((cord[i][0]-5)*coef),((cord[i][1]-5)*coef)]) 
		m = cv2.moments(contours[0]+[((cord[i][0]-5)*coef),((cord[i][1]-5)*coef)])

		realcord = np.vstack([realcord, [int(m["m10"]/m["m00"]),int(m["m01"]/m["m00"])]])	
		 
	scalex = realcord[2][1] - realcord[2][0]
	scaley = realcord[1][1] - realcord[1][0]
	rapperdistance = pow((pow(scalex,2)+pow(scaley,2)),.5)

	rapperpointsimg = {'x1':realcord[1][0], 'y1':realcord[1][1], 'x2': realcord[2][0], 'y2':realcord[2][1], 'dist':rapperdistance}
		
	return(rapperpointsimg)

def getBoardDetection(image):
	
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	gray = cv2.GaussianBlur(gray, (3, 3), 0)

	edged = cv2.Canny(gray, 5, 20)
	showImage(edged)


	kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
	closed = cv2.morphologyEx(edged, cv2.MORPH_CLOSE, kernel)

	# найдите контуры в изображении и подсчитайте количество книг
	cnts = cv2.findContours(closed.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
	cnts = imutils.grab_contours(cnts)
	total = 0

	# цикл по контурам
	for c in cnts:
		# аппроксимируем (сглаживаем) контур
		peri = cv2.arcLength(c, True)
		approx = cv2.approxPolyDP(c, 0.02 * peri, True)
		# если у контура 4 вершины, предполагаем, что это книга
		if len(approx) == 4:
			cv2.drawContours(image, [approx], -1, (0, 255, 0), 4)
			total += 1

	showImage(image)

def drawBadComponents(result, component_type, componentsSMD, board, scalecoefX, scalecoefY):

	#resultimage = board.copy()
	for i in range(len(componentsSMD)):
		if result[componentsSMD[i]['RefDes']][0] == component_type:
			if result[componentsSMD[i]['RefDes']][1][1] == 'bad':
				print(result[componentsSMD[i]['RefDes']])
				x = float(x0) - float(componentsSMD[i]['X'])*scalecoefX 
				y = float(y0) + float(componentsSMD[i]['Y'])*scalecoefY

				resultimage = cv2.circle(board, (int(x),int(y)), 25, (255,0,0), 2)
	showImage(resultimage, size=.5)


#ИНТЕРФЕЙС
calibration = 0
getimages = 0
getcoordinats = 0

#ПАРАМЕТРЫ
num = 15 #Колличество фото для калибровки
imgcoef = .5

#КООРДИНАТЫ РЕППЕРНЫХ ТОЧЕК НА ПЛАТЕ
rapperpoints = {'x1':10.86, 'y1':7.38, 'x2': 170.2, 'y2':83.8, 'dist':176.73}

#КОМПОНЕНТЫ С ДЕФФЕКТАМИ
board_1_deffect = ['VD7','VD8','VD3','VD4','VD29','VD30','VD31','VD32','VD34','VD33', 
				'R19','R21','R46','R47','R48','R49','R50','R51','R55','R56','R57','R58','R59','R60','R72','R73','R74','R75','R76','R77',
				'R67','R90','R106', 
				'VT11', 'VT8', 'VT16',
				'SB10','SB4','SB9','SB3','SB8','SB2']
board_2_deffect = ['VD7','VD8','VD3','VD4','VD31', 'VD32', 'VD33', 'VD34', 'VD57', 'VD20', 'VD21', 'VD22', 
				'R57', 'R48', 'R74', 'R75', 'R49', 'R58', 'R60', 'R51', 'R50', 'R59', 'R77', 'R76', 'R130',
				'VT4', 'VT2', 'VT1', 'VT11', 'VT8',
				'SB10','SB4','SB9','SB3']
board_3_deffect = ['VD7','VD8','VD3','VD4','VD13','VD1','VD31','VD32','VD34','VD33','VD19', 
				'R19','R57','R48','R49','R58','R60','R51','R50','R59','R77','R76',
				'SB10','SB4','SB9','SB3']

#КАЛИБРОВКА КАМЕРЫ
if calibration == 1:
	#chessImage(host, user, secret, port, num) #создание num изображений для калибровки
	chessImage(num) #создание num изображений для калибровки
	calibrationMatrix() #создание матрицы калибровк

#ПОЛУЧЕНИЕ ИЗОБРАЖЕНИЙ
if getimages == 1:

	print ('GET IMAGES... ENTER KEY (0-template, 1...).','\n','OR PRESS ENTER TO EXIT...' )
	key = int(input())
	if key == 0: getImage('projects/PK8-3_v8/template/image/image.npy', 'data.npy') #(localpath, remotepath)
	elif key == 1: getImage('projects/PK8-3_v8/board_1/image/image.npy', 'data.npy') #(localpath, remotepath)
	elif key == 2: getImage('projects/PK8-3_v8/board_2/image/image.npy', 'data.npy') #(localpath, remotepath)
	elif key == 3: getImage('projects/PK8-3_v8/board_3/image/image.npy', 'data.npy') #(localpath, remotepath)	

#ИСПРАВЛЕНИЕ ДИСТОРСИИ В ПРОЕКТАХ
#showImage(cv2.convertScaleAbs(np.load('projects/PK8-3_v8/template/image/image.npy'), alpha = 2, beta = 30), size=.4)
template = imageUndistortion(np.load('projects/PK8-3_v8/template/image/image.npy')) 
board_1 = imageUndistortion(np.load('projects/PK8-3_v8/board_1/image/image.npy')) 
board_2 = imageUndistortion(np.load('projects/PK8-3_v8/board_2/image/image.npy'))
board_3 = imageUndistortion(np.load('projects/PK8-3_v8/board_3/image/image.npy'))
#showImage(cv2.convertScaleAbs(template, alpha = 2, beta = 30), size=.4)
#ВЫБОР ПРОЕКТА 
print ('CHOOSE THE BOARD (1,2,3)')
key_projects = int(input())
projects = [template, board_1, board_2, board_3] 
projects_path = ['/template', '/board_1', '/board_2', '/board_3']
project_dict_deffects = ['None',  board_1_deffect, board_2_deffect, board_3_deffect]

#ОБРАБОТКА ИЗОБРАЖЕНИЙ
template = rotationImage(cv2.convertScaleAbs(template, alpha = 2, beta = 30)) #ПОВОРОТ МАТРИЦЫ
board = rotationImage(cv2.convertScaleAbs(projects[key_projects], alpha = 2, beta = 30))
#showImage(template)

#ПРИМЕНЕНИЕ ФИЛЬТРОВ
templatefilters = changeImage(template) 
board_filters = changeImage(board)

#ПОЛУЧЕНИЕ КООРДИНАТ РЕППЕРНЫХ ТОЧЕК
if getcoordinats == 1:
	print ('GET COORDINATES RAPPER POINTS...')
	getCoordinates(templatefilters, 'projects/PK8-3_v8/template/image/paramsrappoints.npy')

#МАССИВ С ДАННЫМИ ДЛЯ ПОЛУЧЕНИЯ КОМПОНЕНТОВ
paramsrp = np.load('projects/PK8-3_v8/template/image/paramsrappoints.npy', allow_pickle = True)	
	

#ПОЛУЧЕНИЕ КОМПОНЕНТОВ TEMPLATE


templateSMD = getComponents(template, rapperpoints, paramsrp[0], paramsrp[1], paramsrp[2], paramsrp[3], 'projects/PK8-3_v8/template/components/') 
#print(templateSMD['R19VT11'])
#ПОЛУЧЕНИЕ КОМПОНЕНТОВ BOARD
board_SMD = getComponents(board, rapperpoints, paramsrp[0], paramsrp[1], paramsrp[2], paramsrp[3], 'projects/PK8-3_v8/'+projects_path[key_projects]+'/components/')



'''
imgg = np.load('projects/PK8-3_v8/template/components/VD7.npy')
imgg2 = np.load('projects/PK8-3_v8/board_1/components/VD7.npy')
#imgg = cv2.convertScaleAbs(imgg, alpha = 1, beta = 20) #изменение яркости g(x)=αf(x)+β
#imgg2 = cv2.convertScaleAbs(imgg2, alpha = 1, beta = 20) #изменение яркости g(x)=αf(x)+β

imgg = cv2.cvtColor(imgg, cv2.COLOR_BGR2GRAY)
imgg = cv2.medianBlur(imgg, 3)
imgg = cv2.GaussianBlur(imgg,(3,3),0)
	
imgg2 = cv2.cvtColor(imgg2, cv2.COLOR_BGR2GRAY)
imgg2 = cv2.medianBlur(imgg2, 3)
imgg2 = cv2.GaussianBlur(imgg2,(3,3),0)

hist(imgg)
hist(imgg2)

ret, imgg = cv2.threshold(imgg, 50, 255, cv2.THRESH_BINARY)
ret, imgg2 = cv2.threshold(imgg2, 50, 255, cv2.THRESH_BINARY)
#histColor(imgg)
#histColor(imgg2)

showImage(imgg, size=8)
showImage(imgg2, size=8)
'''



#ОБРАБОТКА АЛГОРИТМАМИ
resultTemplate = {}
resultTemplate2 = {}

resultSSIM = {}
resultMSE = {}
resultMSE2 = {}
resultDefChanals = {}
resultSIFT = {}
resultImageHash = {}
resultpHash = {}
result = []

for i in range(len(templateSMD)):

	templatecomp = np.load('projects/PK8-3_v8/template/components/'+templateSMD[i]['RefDes']+'.npy')
	board_comp = np.load('projects/PK8-3_v8/'+projects_path[key_projects]+'/components/'+board_SMD[i]['RefDes']+'.npy')

	#ФОРМИРОВАНИЕ СЛОВАРЯ С РЕАЛЬНЫМ РЕЗУЛЬТАТОМ ПРОВЕРКИ
	resultTemplate[templateSMD[i]['RefDes']] = 'good'

	#resultTemplate2.append({templateSMD[i]['RefDes']:'good', 
							#'Type':templateSMD[i]['Type']})

	resultTemplate2[templateSMD[i]['RefDes']] = [templateSMD[i]['Type'], 'good']	#Type, result


	#ФОРМИРОВАНИЕ СЛОВАРЕЙ С ПРИМЕНЕНИЕМ АЛГОРИТМОВ
	#resultMSE[templateSMD[i]['RefDes']] = Algorithms.algorithmMSE(templatecomp, board_comp)

	resultMSE[templateSMD[i]['RefDes']] = [templateSMD[i]['Type'], 
											Algorithms.algorithmMSE(templatecomp, board_comp, templateSMD[i]['Type'])]
	resultSSIM[templateSMD[i]['RefDes']] = [templateSMD[i]['Type'], 
											Algorithms.algorithmSSIM(templatecomp, board_comp,templateSMD[i]['Type'])]
	resultDefChanals[templateSMD[i]['RefDes']] = [templateSMD[i]['Type'], 
											Algorithms.algorithmDefChanals(templatecomp, board_comp,templateSMD[i]['Type'])]
	resultSIFT[templateSMD[i]['RefDes']] = [templateSMD[i]['Type'], 
											Algorithms.algorithmSIFT(templatecomp, board_comp,templateSMD[i]['Type'])]
	resultImageHash[templateSMD[i]['RefDes']] = [templateSMD[i]['Type'], 
											Algorithms.calcImageHash(templatecomp, board_comp,templateSMD[i]['Type'])]
	resultpHash[templateSMD[i]['RefDes']] = [templateSMD[i]['Type'], 
											Algorithms.pHash(templatecomp, board_comp,templateSMD[i]['Type'])]

	#print ('\n')
	#showImage(np.load('projects/PK8-3_v8/template/components/'+templateSMD[i]['RefDes']+'.npy'), name = templateSMD[i]['RefDes'] , size = 8)
	#showImage(np.load('projects/PK8-3_v8/'+projects_path[key_projects]+'/components/'+board_SMD[i]['RefDes']+'.npy'), name = board_SMD[i]['RefDes'] , size = 8)
	#showImage(np.load('projects/PK8-3_v8/board_2/components/'+board_1SMD[i]['RefDes']+'.npy'), name = board_1SMD[i]['RefDes'] , size = 8)
	#showImage(np.load('projects/PK8-3_v8/board_3/components/'+board_1SMD[i]['RefDes']+'.npy'), name = board_1SMD[i]['RefDes'] , size = 8)

result = [resultpHash, resultImageHash, resultMSE, resultSSIM, resultDefChanals, resultSIFT]
#result = [resultMSE2]


result_names = ['pHash','ImageHash','MSE', 'SSIM', 'DefChanals', 'SIFT']




all_results = {}
all_results_names = {}

all_results = {'805':resultMSE,'1206':resultSSIM,'2010':resultImageHash,
				'SOT23':resultMSE,'SWT-34A':resultMSE,
				'MINIMELF':resultMSE,'DO214AA':resultMSE}

all_results_names = {'805':'MSE','1206':'SSIM','2010':'ImageHash',
				'SOT23':'MSE','SWT-34A':'MSE',
				'MINIMELF':'MSE','DO214AA':'MSE'}

showImage(template, size=.5)
for key, value in all_results.items():
	false_positive, false_negative = histResults2(resultPerfect2(resultTemplate2, 
													project_dict_deffects[key_projects]), 
													all_results[key], 
													key, 
													all_results_names[key])

	drawBadComponents(all_results[key],key, templateSMD, board, paramsrp[2], paramsrp[3])	

'''
#components = ['805','1206','2010','SO8','SO16','SOT23','TSSOP8','SOIC8','LQFP48','SOP4','LQH3N','POWERSO10','SWT-34A','MINIMELF','DO214AA','DO214AB','DO214AC','HC-49S/SMD']
#components = ['805','1206','2010','SO8','SOT23','SWT-34A','MINIMELF','DO214AA']
components = ['MINIMELF']
for alg in range(len(result)):
	for comp in range(len(components)):
		#histResults(resultPerfect(resultTemplate, project_dict_deffects[key_projects]), result[alg], components[comp], result_names[alg])
		false_positive, false_negative = histResults2(resultPerfect2(resultTemplate2, 
													project_dict_deffects[key_projects]), 
													result[alg], 
													components[comp], 
													result_names[alg])
	#drawBadComponents(false_positive, false_negative, templateSMD, board, paramsrp[2], paramsrp[3])

		drawBadComponents(result[alg],components[comp], templateSMD, board, paramsrp[2], paramsrp[3])

	showImage(board, size = .5)
'''


'''
		if len(false_positive) != 0:
			#print ('false positive: ', false_positive)
			for i in range(len(false_positive)):
				showImage(np.load('projects/PK8-3_v8/template/components/'+false_positive[i]+'.npy'), name = false_positive[i] , size = 8)
				showImage(np.load('projects/PK8-3_v8/'+projects_path[key_projects]+'/components/'+false_positive[i]+'.npy'), name = false_positive[i] , size = 8)
				#hist(np.load('projects/PK8-3_v8/template/components/'+false_positive[i]+'.npy'))
				#hist(np.load('projects/PK8-3_v8/'+projects_path[key_projects]+'/components/'+false_positive[i]+'.npy'))
		if len(false_negative) !=0:
			#print ('false negative: ', false_negative)
			for i in range(len(false_negative)):
				showImage(np.load('projects/PK8-3_v8/template/components/'+false_negative[i]+'.npy'), name = false_negative[i] , size = 8)
				showImage(np.load('projects/PK8-3_v8/'+projects_path[key_projects]+'/components/'+false_negative[i]+'.npy'), name = false_negative[i] , size = 8)
				#hist(np.load('projects/PK8-3_v8/template/components/'+false_negative[i]+'.npy'))
				#hist(np.load('projects/PK8-3_v8/'+projects_path[key_projects]+'/components/'+false_negative[i]+'.npy'))

'''
