import paramiko
import pysftp
import time

class CameraImage():

	host = '192.168.100.104'
	user = 'pi'
	secret = 'raspberry'
	port = 22

	def getCameraImg(self):

		self.client = paramiko.SSHClient()
		self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		# Подключение
		self.client.connect(hostname=self.host, username=self.user, password=self.secret, port=self.port)
		# Выполнение команды
		self.client.exec_command('raspiyuv -o cam.jpg -ISO 1000')
		#self.stdin, self.stdout, self.stderr = self.client.exec_command('raspistill -o cam.jpg')
		self.stdin, self.stdout, self.stderr = self.client.exec_command('ls')
		self.data = self.stdout.read()

		while ('cam.jpg' in self.data.decode("utf-8"))!=True:
			time.sleep(1)
			self.stdin, self.stdout, self.stderr = self.client.exec_command('ls')
			self.data = self.stdout.read()

		self.client.close()

		time.sleep(1)

		self.cnopts = pysftp.CnOpts()
		self.cnopts.hostkeys = None  
		with pysftp.Connection(self.host, username=self.user, password=self.secret, cnopts=self.cnopts) as self.sftp:
			self.sftp.get('cam.jpg')
			self.sftp.remove('cam.jpg')
			self.sftp.close()

class RaspberryPi():
	"""docstring"""
	
	def __init__(self, host, user, secret, port):

		self.host = host
		self.user = user
		self.secret = secret
		self.port = port
		#self.command = command

	def commandSSH(self, command):

		self.client = paramiko.SSHClient()
		self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

		# Подключение
		self.client.connect(hostname=self.host, username=self.user, password=self.secret, port=self.port)

		# Выполнение команды
		self.stdin, self.stdout, self.stderr = self.client.exec_command(command)

		return (self.stdout.read())

		self.client.close()

		
	def copyData(self, remotepath, localpath):

		self.cnopts = pysftp.CnOpts()
		self.cnopts.hostkeys = None  
		with pysftp.Connection(self.host, username=self.user, password=self.secret, cnopts=self.cnopts) as self.sftp:
			self.sftp.get(remotepath, localpath)
			self.sftp.remove(remotepath)
			self.sftp.close()

class Point(): #УДАЛИТЬ

	def __init__ (self):
		pass

	def drawPoint():
		pass

	def findCenter():
		pass

class Windows():

	def __init__ (self):
		pass

	def showImage(self, image, name = 'Image', size = .5, mouse = False):

		resize = cv2.resize(image, (0,0), fx=size, fy=size)
		cv2.imshow(name, resize)

		if mouse == True: 
			cv2.setMouseCallback(name, getCoordinats)

		cv2.waitKey()
		cv2.destroyAllWindows()

	def draw():
		pass	

	def getPoints(event,x,y,flags,param):
		pass


#test = CameraImage()
#test.getCameraImg()