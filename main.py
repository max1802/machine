import Modules
import Algorithms


import numpy as np
import matplotlib.pyplot as plt

import cv2
import glob

import csv

import os

#Modules.CameraImage().getCameraImg()

#настройки для подключения к Raspberry
host = '192.168.100.104'
user = 'pi'
secret = 'raspberry'
port = 22

cord = np.zeros(shape=2, dtype=int)

#ОБРАБОТКА СОБЫТИЙ
def getCoordinats(event,x,y,flags,param):

	if event == cv2.EVENT_LBUTTONDBLCLK:
		global cord
		cord = np.vstack([cord, [x,y]])
		print (cord)

#КЛАСС RASPBERRY
def chessImage(num):

	chess = Modules.RaspberryPi(host, user, secret, port)
	for i in range(num):

		if i == 0:
			print ('Set the chess...')
		else:
			print ('Move the chess...')

		input()
		print ('OK')

		chess.commandSSH('raspistill -o chess.jpg')
		localpath = 'chess/'+'chess_' + str(i) + '.jpg'
		chess.copyData('chess.jpg', localpath)

def getImage(localpath, remotepath):

	image = Modules.RaspberryPi(host, user, secret, port)
	#image.commandSSH('python bayer.py')
	image.commandSSH('python bayer_demosaic.py')
	image.copyData(remotepath, localpath)

#КЛАСС ИЗОБРАЖЕНИЯ
def calibrationMatrix():
	# termination criteria
	criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

	# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
	objp = np.zeros((6*9,3), np.float32)
	objp[:,:2] = np.mgrid[0:9,0:6].T.reshape(-1,2)

	# Arrays to store object points and image points from all the images.
	objpoints = [] # 3d point in real world space
	imgpoints = [] # 2d points in image plane.

	images = glob.glob('chess/*.jpg')

	for fname in images:
	    img = cv2.imread(fname)
	    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

	    # Find the chess board corners
	    ret, corners = cv2.findChessboardCorners(gray, (9,6),None)

	    # If found, add object points, image points (after refining them)
	    if ret == True:
	        objpoints.append(objp)
	        corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
	        imgpoints.append(corners2)

	        # Draw and display the corners
	        #img = cv2.drawChessboardCorners(img, (9,6), corners2,ret)
	        #small = cv2.resize(img, (0,0), fx=0.5, fy=0.5)
	        #cv2.imshow('img',small)
	        #cv2.waitKey()

	#cv2.destroyAllWindows()
	ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],None,None)

	np.save('chess/ret', ret)
	np.save('chess/mtx', mtx)
	np.save('chess/dist', dist)
	np.save('chess/rvecs', rvecs)
	np.save('chess/tvecs', tvecs)

def imageUndistortion(img):

	#img = cv2.imread('1.jpg')
	h,  w = img.shape[:2]
	
	newcameramtx, roi=cv2.getOptimalNewCameraMatrix(np.load('chess/mtx.npy'),np.load('chess/dist.npy'),(w,h),1,(w,h))
	#newcameramtx, roi=cv2.getOptimalNewCameraMatrix(mtx,dist,(w,h),1,(w,h))

	mapx,mapy = cv2.initUndistortRectifyMap(np.load('chess/mtx.npy'),np.load('chess/dist.npy'),None,newcameramtx,(w,h),5)
	dst = cv2.remap(img,mapx,mapy,cv2.INTER_LINEAR)

	# crop the image
	x,y,w,h = roi
	dst = dst[y:y+h, x:x+w]
	#cv2.imwrite('2.jpg',dst)
	return (dst)

def showImage(image, name = 'Image', size = .5, mouse = False):

	image = cv2.resize(image, (0,0), fx=size, fy=size)
	image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
	cv2.imshow(name, image)

	if mouse == True: 
		cv2.setMouseCallback(name, getCoordinats)

	cv2.waitKey()
	cv2.destroyAllWindows()

def changeImage(image):



	image = cv2.convertScaleAbs(image, alpha = 2, beta = 30) #изменение яркости g(x)=αf(x)+β
	#image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

	'''
	image2 = image.copy()
	#image2 = np.zeros((len(image), len(image[0]), 3), dtype=int)
	for i in range(len(image)):
		for j in range(len(image[0])):
			for k in range(3):
				if image[i][j][k] > 120:
					image2[i][j][k] = image2[i][j][k]-50
	'''

	image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) #Greylevel = 0.299R + 0.587G + 0.114B
	clahe = cv2.createCLAHE(clipLimit=8.0, tileGridSize=(8,8))
	image = clahe.apply(image)

	#image = cv2.medianBlur(image, 3)
	#image = cv2.GaussianBlur(image,(3,3),0)
	#ret, image = cv2.threshold(image, 150, 150, cv2.THRESH_TRUNC)

	ret, image = cv2.threshold(image, 140, 255, 0)

	'''
	image = cv2.Canny(image,50,150,apertureSize = 3)

	lines = cv2.HoughLines(image,1,np.pi/180, 200)

	for r,theta in lines[0]:
		# Сохраняет значение cos (theta) в
		a = np.cos(theta)
		# Сохраняет значение греха (тета) в б
		b = np.sin(theta)
		# x0 хранит значение rcos (theta)
		x0 = a*r
		# y0 хранит значение rsin (theta)
		y0 = b*r
		# x1 хранит округленное значение (rcos (theta) -1000sin (theta))
		x1 = int(x0 + 1000*(-b))
		# y1 хранит округленное значение (rsin (тета) + 1000cos (тета))
		y1 = int(y0 + 1000*(a))
		# x2 хранит округленное значение (rcos (тета) + 1000sin (тета))
		x2 = int(x0 - 1000*(-b))
		# y2 хранит округленное значение (rsin (theta) -1000cos (theta))
		y2 = int(y0 - 1000*(a))
		# cv2.line рисует линию в img из точки (x1, y1) в (x2, y2).
		# (0,0,255) обозначает цвет линии, которая будет
		#drawn. В этом случае это красный.
		cv2.line(image,(x1,y1), (x2,y2), (0,0,255),2)

	cv2.imwrite('linesDetected.jpg', image)
	'''
	return(image)

def rotationImage(image):
    (h, w) = image.shape[:2]
    center = (int(w / 2), int(h / 2))
    rotation_matrix = cv2.getRotationMatrix2D(center, -.6, 1)
    image = cv2.warpAffine(image, rotation_matrix, (w, h))

    return(image)

def getComponents(image, rapperpoints, rapperpointsimg, scalecoef, scalecoefX, scalecoefY, path):

	sizeSMD = [] #Создаем словарь для размеров SMD корпусов
	componentsSMD = [] #Создаем словарь для компонентов проекта

	#Открываем csv файл и копируем значения в виде словаря каждую строку таблицы
	with open('base/sizeSMD.csv') as File:
		reader = csv.DictReader(File, delimiter=';')
		for row in reader:
			sizeSMD.append(row)

	#Ищем в массиве нужный словарь по паре значений Ключ-Значение 
	#print(next(item for item in sizeSMD if item['Body'] == '805'))

	#Открываем csv файл с компонентами проекта
	with open('projects/PK8-3_v8/componentsSMD.csv') as File:
		reader = csv.DictReader(File, delimiter=';')
		for row in reader:
			componentsSMD.append(row)

	#Рассчет крайних точек компонента (10 это поправочный, надо бы удалить)
	x0 = (rapperpointsimg['x1']+(rapperpoints['x1'])*scalecoefX) 
	y0 = (rapperpointsimg['y1']-(rapperpoints['y1'])*scalecoefY) 

	#Цикл для получения изображений компонентов
	for i in range(len(componentsSMD)):
		if componentsSMD[i]['Rotate'] == '0' or componentsSMD[i]['Rotate'] == '180':
			bodyX = float((next(item for item in sizeSMD if item['Body'] == componentsSMD[i]['Type']))['Width'])
			bodyY = float((next(item for item in sizeSMD if item['Body'] == componentsSMD[i]['Type']))['Height'])
		else:
			bodyX = float((next(item for item in sizeSMD if item['Body'] == componentsSMD[i]['Type']))['Height'])
			bodyY = float((next(item for item in sizeSMD if item['Body'] == componentsSMD[i]['Type']))['Width'])			

		x1 = float(x0) - float(componentsSMD[i]['X'])*scalecoefX - scalecoefX*bodyX
		x2 = float(x0) - float(componentsSMD[i]['X'])*scalecoefX + scalecoefX*bodyX
		y1 = float(y0) + float(componentsSMD[i]['Y'])*scalecoefY - scalecoefY*bodyY
		y2 = float(y0) + float(componentsSMD[i]['Y'])*scalecoefY + scalecoefY*bodyY		
		np.save(path+componentsSMD[i]['RefDes'], image[int(y1):int(y2), int(x1):int(x2)])
		#np.save('data/'+str(i), image[int(y1):int(y2), int(x1):int(x2)])

	return(componentsSMD)

def getCoordinates(templatefilters,path):

	showImage(templatefilters, size = imgcoef, mouse = True) #ВЫВОД ИЗОБРАЖЕНИЯ ДЛЯ ПОЛУЧЕНИЯ КООРДИНАТ

	#ПОЛУЧЕНИЕ КООРДИНАТ ЧЕРЕЗ ЦЕНТР МАССЫ
	rapperpointsimg = realPoints(templatefilters, rapperpoints)
	#rapperpointsimg = realPoints(board_1filters, rapperpoints) 
	#rapperpointsimg = realPoints(board_2filters, rapperpoints) 
	#rapperpointsimg = realPoints(board_3filters, rapperpoints) 

	scalecoef = rapperpointsimg['dist']/rapperpoints['dist']
	scalecoefX = (rapperpointsimg['x1']-rapperpointsimg['x2'])/(rapperpoints['x2']-rapperpoints['x1'])
	scalecoefY = (rapperpointsimg['y2']-rapperpointsimg['y1'])/(rapperpoints['y2']-rapperpoints['y1'])

	#print(rapperpointsimg['x2']-rapperpointsimg['x1'])
	#print (rapperpoints['x2']-rapperpoints['x1'])
	a = np.array([rapperpointsimg ,scalecoef, scalecoefX, scalecoefY])

	np.save(path, a)

	#np.save('projects/PK8-3_v8/template/image/paramsrappoints.npy', a)
	#np.save('projects/PK8-3_v8/board_1/image/paramsrappoints.npy', a)
	#np.save('projects/PK8-3_v8/board_2/image/paramsrappoints.npy', a)
	#np.save('projects/PK8-3_v8/board_3/image/paramsrappoints.npy', a)

	#ОТОБРАЖЕНИЕ РЕПЕРНЫХ ТОЧЕК
	for i in range(1,len(cord)):
		img = cv2.circle(template, (rapperpointsimg['x'+str(i)],rapperpointsimg['y'+str(i)]), 5, (0,0,255), -1)
		img = cv2.circle(template, (220,260), 5, (0,0,255), -1)
		img = cv2.circle(template, (1800,260), 5, (0,0,255), -1)
		img = cv2.circle(template, (220,1260), 5, (0,0,255), -1)
		img = cv2.circle(template, (1800,1260), 5, (0,0,255), -1)

	showImage(img) #ПОКАЗАТЬ ИЗОБРАЖЕНИЕ

#ВСПОМОГАТЕЛЬНЫЕ ФУНКЦИИ:
def hist(image_gaussian):
	histr = cv2.calcHist([image_gaussian],[0],None,[256],[0,256])
	plt.plot(histr)
	plt.show()

def histResults(dict_template, dict_testimage, component_type, algorithm_type):

	recognized = 0
	false_positive = 0
	false_negative = 0
	hist = []
	hist2 = []

	for key, value in dict_template.items():

		
		#hist.append(dict_testimage[key][0])

		if key[:(len(component_type))] == component_type:
			if dict_template[key] == dict_testimage[key][1]:
				recognized+=1
				
			elif dict_template[key] == 'bad' and dict_testimage[key][1] == 'good':
				false_positive+=1 
			elif dict_template[key] == 'good' and dict_testimage[key][1] == 'bad':
				false_negative+=1

			if dict_template[key] == 'good': hist.append(dict_testimage[key][0])
			else: hist2.append(dict_testimage[key][0])
	
	#print (hist)	 
	value = (recognized, false_positive, false_negative) 
	position = np.arange(3)
	fig, ax = plt.subplots()
	ax.bar(position, value)
	ax.set_title(algorithm_type + '\n' + component_type)
	#  Устанавливаем позиции тиков:
	ax.set_xticks(position)

	#  Устанавливаем подписи тиков
	ax.set_xticklabels(['Распознаные'+str(recognized),
						'Ложноположительные'+str(false_positive), 
						'Ложноотрицательные'+str(false_negative)])
	fig.set_figwidth(8)
	fig.set_figheight(6)
	plt.show()

	plt.boxplot([hist, hist2])
	plt.show()


def resultPerfect(dict_template, board_deffect):
	#Обозначение компонентов, которые испорчены

	for i in range(len(board_deffect)):

		dict_template[board_deffect[i]] = 'bad'

	return (dict_template)

#ТЕСТОВЫЕ ФУНКЦИИ:
def realPoints(image, rapperpoints ,size = .5):

	newcontours = []
	m = []
	realcord = np.zeros(shape=2, dtype=int)
	coef = int(pow(size, -1))

	for i in range(1, len(cord)):
		contours, hierarchy = cv2.findContours(image[((cord[i][1]-5)*coef):((cord[i][1]+5)*coef),
			((cord[i][0]-5)*coef):((cord[i][0]+5)*coef)], 
			cv2.RETR_EXTERNAL, 
			cv2.CHAIN_APPROX_SIMPLE)

		newcontours.append(contours[0]+[((cord[i][0]-5)*coef),((cord[i][1]-5)*coef)]) 
		m = cv2.moments(contours[0]+[((cord[i][0]-5)*coef),((cord[i][1]-5)*coef)])

		realcord = np.vstack([realcord, [int(m["m10"]/m["m00"]),int(m["m01"]/m["m00"])]])	
		 
	scalex = realcord[2][1] - realcord[2][0]
	scaley = realcord[1][1] - realcord[1][0]
	rapperdistance = pow((pow(scalex,2)+pow(scaley,2)),.5)

	rapperpointsimg = {'x1':realcord[1][0], 'y1':realcord[1][1], 'x2': realcord[2][0], 'y2':realcord[2][1], 'dist':rapperdistance}
	
	#cv2.circle(good, (realcord[i][0],realcord[i][1]), 5, (255,0,0), -1)
	#cv2.drawContours(good, newcontours, -1, (255, 0, 0), 10)
		
	return(rapperpointsimg)

def drawImage():
	pass

def getBoardDetection():

	
#ИНТЕРФЕЙС
calibration = 0
getimages = 0
getcoordinats = 1

#ПАРАМЕТРЫ
num = 10 #Колличество фото для калибровки
imgcoef = .5

#КОМПОНЕНТЫ С ДЕФФЕКТАМИ
board_1deffect = ['VD7','VD8','VD3','VD4','VD29','VD30','VD31','VD32','VD34','VD33', 
				'R72','R73','R74','R55','R46','R47','R56','R57','R48','R49','R58','R60','R51','R59','R77','R76','R21','R19'
				'VT11', 'VT8',
				'SB10','SB4','SB9','SB3','SB8','SB2']
board_1deffect = ['VD7','VD8','VD3','VD4','VD31', 'VD32', 'VD33', 'VD34', 'VD57', 'VD20', 'VD21', 'VD22', 
				'R57', 'R48', 'R74', 'R75', 'R49', 'R58', 'R60', 'R51', 'R50', 'R59', 'R77', 'R76', 'R130'
				'VT4', 'VT2', 'VT1', 'VT11', 'VT8',
				'SB10','SB4','SB9','SB3']
board_1deffect = ['VD7','VD8','VD3','VD4','VD13','VD1','VD31','VD32','VD34','VD33','VD19', 
				'R19','R57','R48','R49','R58','R60','R51','R50','R59','R77','R76',
				'SB10','SB4','SB9','SB3']

#realscalex = 159.4 #Расстояние между репперными точками по Х
#realscaley = 766.5 #Расстояние между репперными точками по У
#realscale = 176.81 #Расстояние между репперными точками 
#mainrapperpoint = [10.8, 7.4]
rapperpoints = {'x1':10.86, 'y1':7.38, 'x2': 170.2, 'y2':83.8, 'dist':176.73}

#КАЛИБРОВКА КАМЕРЫ
if calibration == 1:
	chessImage(host, user, secret, port, num) #создание num изображений для калибровки
	calibrationMatrix() #создание матрицы калибровк

#ПОЛУЧЕНИЕ ИЗОБРАЖЕНИЙ
if getimages == 1:
	#getImage('templates/good200.npy', 'data.npy') #(localpath, remotepath)

	#getImage('projects/PK8-3_v8/template/image/image.npy', 'data.npy') #(localpath, remotepath)
	#getImage('projects/PK8-3_v8/board_1/image/image.npy', 'data.npy') #(localpath, remotepath)
	#getImage('projects/PK8-3_v8/board_2/image/image.npy', 'data.npy') #(localpath, remotepath)
	getImage('projects/PK8-3_v8/board_3/image/image.npy', 'data.npy') #(localpath, remotepath)	
	#getImage('templates/bed.npy', 'data.npy') #(localpath, remotepath)

template = imageUndistortion(np.load('projects/PK8-3_v8/template/image/image.npy')) #ИСПРАВЛЕНИЕ ДИСТОРСИИ
board_1 = imageUndistortion(np.load('projects/PK8-3_v8/board_1/image/image.npy')) #ИСПРАВЛЕНИЕ ДИСТОРСИИ
board_2 = imageUndistortion(np.load('projects/PK8-3_v8/board_2/image/image.npy')) #ИСПРАВЛЕНИЕ ДИСТОРСИИ
board_3 = imageUndistortion(np.load('projects/PK8-3_v8/board_3/image/image.npy')) #ИСПРАВЛЕНИЕ ДИСТОРСИИ

#template = np.load('templates/good.npy') #загрузка трехмерного массива 2592х1944х3
'''
template = rotationImage(template) #ПОВОРОТ МАТРИЦЫ
board_1 = rotationImage(board_1)
board_2 = rotationImage(board_2)
board_3 = rotationImage(board_3)
'''

template = rotationImage(cv2.convertScaleAbs(template, alpha = 2, beta = 30)) #ПОВОРОТ МАТРИЦЫ
board_1 = rotationImage(cv2.convertScaleAbs(board_1, alpha = 2, beta = 30))
board_2 = rotationImage(cv2.convertScaleAbs(board_2, alpha = 2, beta = 30))
board_3 = rotationImage(cv2.convertScaleAbs(board_3, alpha = 2, beta = 30))

templatefilters = changeImage(template) #ПРИМЕНЕНИЕ ФИЛЬТРОВ
board_1filters = changeImage(board_1)
board_2filters = changeImage(board_2)
board_3filters = changeImage(board_3)

#getCoordinates(templatefilters,'projects/PK8-3_v8/template/image/paramsrappoints.npy')
#getCoordinates(board_1filters,'projects/PK8-3_v8/board_1/image/paramsrappoints.npy')
#getCoordinates(board_2filters,'projects/PK8-3_v8/board_2/image/paramsrappoints.npy')
#getCoordinates(board_3filters,'projects/PK8-3_v8/board_3/image/paramsrappoints.npy')


#hist(templatefilters)

'''
#ПОЛУЧЕНИЕ КООРДИНАТ РЕППЕРНЫХ ТОЧЕК
if getcoordinats == 1:
	showImage(templatefilters, size = imgcoef, mouse = True) #ВЫВОД ИЗОБРАЖЕНИЯ ДЛЯ ПОЛУЧЕНИЯ КООРДИНАТ

	#ПОЛУЧЕНИЕ КООРДИНАТ ЧЕРЕЗ ЦЕНТР МАССЫ
	#rapperpointsimg = realPoints(templatefilters, rapperpoints)
	rapperpointsimg = realPoints(board_1filters, rapperpoints) 
	#rapperpointsimg = realPoints(board_2filters, rapperpoints) 
	#rapperpointsimg = realPoints(board_3filters, rapperpoints) 

	scalecoef = rapperpointsimg['dist']/rapperpoints['dist']
	scalecoefX = (rapperpointsimg['x1']-rapperpointsimg['x2'])/(rapperpoints['x2']-rapperpoints['x1'])
	scalecoefY = (rapperpointsimg['y2']-rapperpointsimg['y1'])/(rapperpoints['y2']-rapperpoints['y1'])

	#print(rapperpointsimg['x2']-rapperpointsimg['x1'])
	#print (rapperpoints['x2']-rapperpoints['x1'])
	a = np.array([rapperpointsimg ,scalecoef, scalecoefX, scalecoefY])

	#np.save('projects/PK8-3_v8/template/image/paramsrappoints.npy', a)
	np.save('projects/PK8-3_v8/board_1/image/paramsrappoints.npy', a)
	#np.save('projects/PK8-3_v8/board_2/image/paramsrappoints.npy', a)
	#np.save('projects/PK8-3_v8/board_3/image/paramsrappoints.npy', a)

	#ОТОБРАЖЕНИЕ РЕПЕРНЫХ ТОЧЕК
	for i in range(1,len(cord)):
		img = cv2.circle(template, (rapperpointsimg['x'+str(i)],rapperpointsimg['y'+str(i)]), 5, (0,0,255), -1)
		img = cv2.circle(template, (220,260), 5, (0,0,255), -1)
		img = cv2.circle(template, (1800,260), 5, (0,0,255), -1)
		img = cv2.circle(template, (220,1260), 5, (0,0,255), -1)
		img = cv2.circle(template, (1800,1260), 5, (0,0,255), -1)

	showImage(img) #ПОКАЗАТЬ ИЗОБРАЖЕНИЕ
'''
#ПРОВЕРКА ПАПКИ НА ПУСТОТУ
#if len(os.listdir('projects/PK8-3_v8/template/components')) == 0:

	
#МАССИВ С ДАННЫМИ ДЛЯ ПОЛУЧЕНИЯ КОМПОНЕНТОВ
paramsrp_temp = np.load('projects/PK8-3_v8/template/image/paramsrappoints.npy', allow_pickle = True)	
paramsrp_board_1 = np.load('projects/PK8-3_v8/board_1/image/paramsrappoints.npy', allow_pickle = True)	
paramsrp_board_2 = np.load('projects/PK8-3_v8/board_2/image/paramsrappoints.npy', allow_pickle = True)	
paramsrp_board_3 = np.load('projects/PK8-3_v8/board_3/image/paramsrappoints.npy', allow_pickle = True)	

#ПОЛУЧЕНИЕ КОМПОНЕНТОВ TEMPLATE
templateSMD = getComponents(template, rapperpoints, paramsrp_temp[0], paramsrp_temp[1], paramsrp_temp[2], paramsrp_temp[3], 'projects/PK8-3_v8/template/components/') 

#ПОЛУЧЕНИЕ КОМПОНЕНТОВ BOARD
'''
board_1SMD = getComponents(board_1, rapperpoints, paramsrp_board_1[0], paramsrp_board_1[1], paramsrp_board_1[2], paramsrp_board_1[3], 'projects/PK8-3_v8/board_1/components/') 
board_2SMD = getComponents(board_2, rapperpoints, paramsrp_board_2[0], paramsrp_board_2[1], paramsrp_board_2[2], paramsrp_board_2[3], 'projects/PK8-3_v8/board_2/components/') 
board_3SMD = getComponents(board_3, rapperpoints, paramsrp_board_3[0], paramsrp_board_3[1], paramsrp_board_3[2], paramsrp_board_3[3], 'projects/PK8-3_v8/board_3/components/') 
'''
board_1SMD = getComponents(board_1, rapperpoints, paramsrp_temp[0], paramsrp_temp[1], paramsrp_temp[2], paramsrp_temp[3], 'projects/PK8-3_v8/board_1/components/') 
board_2SMD = getComponents(board_2, rapperpoints, paramsrp_temp[0], paramsrp_temp[1], paramsrp_temp[2], paramsrp_temp[3], 'projects/PK8-3_v8/board_2/components/') 
board_3SMD = getComponents(board_3, rapperpoints, paramsrp_temp[0], paramsrp_temp[1], paramsrp_temp[2], paramsrp_temp[3], 'projects/PK8-3_v8/board_3/components/') 


resultTemplate = {}
resultSSIM = {}
resultMSE = {}
resultDefChanals = {}
resultSIFT = {}
result = []

for i in range(len(templateSMD)):

	templatecomp = np.load('projects/PK8-3_v8/template/components/'+templateSMD[i]['RefDes']+'.npy')
	board_1comp = np.load('projects/PK8-3_v8/board_1/components/'+board_1SMD[i]['RefDes']+'.npy')
	board_2comp = np.load('projects/PK8-3_v8/board_2/components/'+board_1SMD[i]['RefDes']+'.npy')
	board_3comp = np.load('projects/PK8-3_v8/board_3/components/'+board_1SMD[i]['RefDes']+'.npy')

	#ФОРМИРОВАНИЕ СЛОВАРЯ С РЕАЛЬНЫМ РЕЗУЛЬТАТОМ ПРОВЕРКИ
	resultTemplate[templateSMD[i]['RefDes']] = 'good'

	#ФОРМИРОВАНИЕ СЛОВАРЕЙ С ПРИМЕНЕНИЕМ АЛГОРИТМОВ
	resultMSE[templateSMD[i]['RefDes']] = Algorithms.algorithmMSE(templatecomp, board_1comp)
	resultSSIM[templateSMD[i]['RefDes']] = Algorithms.algorithmSSIM(templatecomp, board_1comp)
	resultDefChanals[templateSMD[i]['RefDes']] = Algorithms.algorithmDefChanals(templatecomp, board_1comp)
	resultSIFT[templateSMD[i]['RefDes']] = Algorithms.algorithmSIFT(templatecomp, templatecomp)


	#print ('\n')
	#showImage(np.load('projects/PK8-3_v8/template/components/'+templateSMD[i]['RefDes']+'.npy'), name = templateSMD[i]['RefDes'] , size = 8)
	#showImage(np.load('projects/PK8-3_v8/board_1/components/'+board_1SMD[i]['RefDes']+'.npy'), name = board_1SMD[i]['RefDes'] , size = 8)
	#showImage(np.load('projects/PK8-3_v8/board_2/components/'+board_1SMD[i]['RefDes']+'.npy'), name = board_1SMD[i]['RefDes'] , size = 8)
	#showImage(np.load('projects/PK8-3_v8/board_3/components/'+board_1SMD[i]['RefDes']+'.npy'), name = board_1SMD[i]['RefDes'] , size = 8)


result = [resultMSE, resultSSIM, resultDefChanals, resultSIFT]
result_names = ['MSE', 'SSIM', 'DefChanals', 'SIFT']
components = ['R','VD','SB','VT']	

for alg in range(len(result)):
	for comp in range(len(components)):
		histResults(resultPerfect(resultTemplate, board_1deffect), result[alg], components[comp], result_names[alg])
