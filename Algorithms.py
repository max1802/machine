from skimage.metrics import structural_similarity as ssim
import matplotlib.pyplot as plt
import numpy as np
import cv2
from PIL import Image
from diffimg import diff

import difflib

import imagehash
import inspect

def algorithmMSE(imageA, imageB, type_component):
	#https://www.pyimagesearch.com/2014/09/15/python-compare-two-images/

	# the 'Mean Squared Error' between the two images is the
	# sum of the squared difference between the two images;
	# NOTE: the two images must have the same dimension

	#Хотя MSE вычисляется значительно быстрее, он имеет главный недостаток: 
	#(1) применяется глобально и (2) оценивается только воспринимаемые 
	#ошибки изображения.

	values = {'805':1370,'1206':340,'2010':900,'SOT23':1000,'SWT-34A':1000,'MINIMELF':2400,'DO214AA':800,
			'TSSOP8':1300,'SOIC8':1300,'LQFP48':1300,'SOP4':1300,'LQH3N':1300,'POWERSO10':1300,'DO214AB':1300,
			'DO214AC':1300,'HC-49S/SMD':1300, 'SMD-C':1300, 'HC-49S/SMD':1300,'SO8':1300,'SO16':1300}


	imageA = cv2.cvtColor(imageA, cv2.COLOR_BGR2GRAY)
	imageB = cv2.cvtColor(imageB, cv2.COLOR_BGR2GRAY)

	err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
	err /= float(imageA.shape[0] * imageA.shape[1])
	
	# return the MSE, the lower the error, the more "similar"
	# the two images are
	#print('MSE: ', err)


	if err > values[type_component]:
		return err, 'bad'	
	return err, 'good'


def algorithmSSIM(imageA, imageB, type_component):
	#https://www.pyimagesearch.com/2014/09/15/python-compare-two-images/

	# compute the mean squared error and structural similarity
	# index for the images

	#С другой стороны, SSIM, хотя и медленнее, может воспринимать изменение 
	#структурной информации изображения, сравнивая локальные области  изображения, 
	#а не глобально.
	values = {'805':.47,'1206':.5,'2010':.65,'SOT23':.56,'SWT-34A':.62,'MINIMELF':.51,'DO214AA':.65,
			'TSSOP8':1300,'SOIC8':1300,'LQFP48':1300,'SOP4':1300,'LQH3N':1300,'POWERSO10':1300,'DO214AB':1300,
			'DO214AC':1300,'HC-49S/SMD':1300, 'SMD-C':1300, 'HC-49S/SMD':1300,'SO8':1300,'SO16':1300}

	imageA = cv2.cvtColor(imageA, cv2.COLOR_BGR2GRAY)
	imageB = cv2.cvtColor(imageB, cv2.COLOR_BGR2GRAY)
	s = ssim(imageA, imageB)

	#print('SSIM: ',s)

	if s < values[type_component]:
		return s, 'bad'	
	return s, 'good'


def algorithmDefChanals(component1, component2, type_component):
	#https://github.com/nicolashahn/diffimg
	
	#на вход передаем пути к компонентам или сами компоненты
	values = {'805':.07,'1206':.05,'2010':.06,'SOT23':.07,'SWT-34A':.08,'MINIMELF':.123,'DO214AA':.07,
			'TSSOP8':1300,'SOIC8':1300,'LQFP48':1300,'SOP4':1300,'LQH3N':1300,'POWERSO10':1300,'DO214AB':1300,
			'DO214AC':1300,'HC-49S/SMD':1300, 'SMD-C':1300, 'HC-49S/SMD':1300,'SO8':1300,'SO16':1300}

	(Image.fromarray(component1)).save('cashe/component1.jpg')
	(Image.fromarray(component2)).save('cashe/component2.jpg')

	result = diff('cashe/component1.jpg', 'cashe/component2.jpg', delete_diff_file=True)
	#result = diff("C2.jpg", "C2.jpg")

	#print('DEF: ',result)

	if result > values[type_component]:
		return result, 'bad'	
	return result, 'good'


def algorithmSIFT(component1, component2, type_component):

	values = {'805':4,'1206':4,'2010':10,'SOT23':2,'SWT-34A':60,'MINIMELF':8,'DO214AA':6,
			'TSSOP8':1300,'SOIC8':1300,'LQFP48':1300,'SOP4':1300,'LQH3N':1300,'POWERSO10':1300,'DO214AB':1300,
			'DO214AC':1300,'HC-49S/SMD':1300, 'SMD-C':1300, 'HC-49S/SMD':1300,'SO8':1300,'SO16':1300}

	imageA = cv2.cvtColor(component1, cv2.COLOR_BGR2GRAY)
	imageB = cv2.cvtColor(component2, cv2.COLOR_BGR2GRAY)


	# Initiate SIFT detector
	sift = cv2.SIFT_create()

	# find the keypoints and descriptors with SIFT
	kp1, des1 = sift.detectAndCompute(imageA,None)
	kp2, des2 = sift.detectAndCompute(imageB,None)

	# BFMatcher with default params
	bf = cv2.BFMatcher()
	matches = bf.knnMatch(des1,des2,k=2)

	if len(matches) == 1:
		return 1, 'bad'	
	# Apply ratio test
	good = []

	for m,n in matches:
		if m.distance < 0.75*n.distance:
			#print (m)
			good.append([m])
			
	# cv.drawMatchesKnn expects list of lists as matches.
	img3 = cv2.drawMatchesKnn(imageA,kp1,imageB,kp2,good,None,flags=cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)
	#plt.imshow(img3),plt.show()

	#print('SIFT: ',len(good))

	if len(good) < values[type_component]:
		return len(good), 'bad'	
	return len(good), 'good'

	'''
	KNN = 2
	LOWE = 0.7
	TREES = 5
	CHECKS = 50

	matcher = cv.FlannBasedMatcher({'algorithm': 0, 'trees': TREES}, {'checks': CHECKS})
	matches = matcher.knnMatch(des1, des2, k=KNN)

	positive = []
	for img11, img22 in matches:
	    if img11.distance < LOWE * img22.distance:
	        positive.append(img22)


	print (positive)

	img3 = cv.drawMatchesKnn(img1,kp1,img2,kp2,good,None,flags=cv.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)

	img3 = cv.drawMatchesKnn(img1,kp1,img2,kp2,good,None,flags=cv.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)
	plt.imshow(img3),plt.show()
	'''



#Функция вычисления хэша
def calcImageHash(component1, component2, type_component):
	#https://wiki.programstore.ru/python-sravnenie-kartinok/
	#http://xlench.bget.ru/doku.php/p/algo/image-comparing
	values = {'805':15,'1206':18,'2010':13,'SOT23':9,'SWT-34A':8,'MINIMELF':13,'DO214AA':10,
			'TSSOP8':1300,'SOIC8':1300,'LQFP48':1300,'SOP4':1300,'LQH3N':1300,'POWERSO10':1300,'DO214AB':1300,
			'DO214AC':1300,'HC-49S/SMD':1300, 'SMD-C':1300, 'HC-49S/SMD':1300,'SO8':1300,'SO16':1300}

	image = component1 #Прочитаем картинку
	resized = cv2.resize(image, (8,8), interpolation = cv2.INTER_AREA) #Уменьшим картинку
	gray_image = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY) #Переведем в черно-белый формат

	#gray_image = cv2.medianBlur(gray_image, 3)
	#gray_image = cv2.GaussianBlur(gray_image,(3,3),0)

	avg=gray_image.mean() #Среднее значение пикселя
	ret, threshold_image = cv2.threshold(gray_image, avg, 255, 0) #Бинаризация по порогу
	#Рассчитаем хэш
	_hash1=""
	for x in range(8):
		for y in range(8):
			val=threshold_image[x,y]
			if val==255:
				_hash1=_hash1+"1"
			else:
				_hash1=_hash1+"0"
     

	image = component2 #Прочитаем картинку
	resized = cv2.resize(image, (8,8), interpolation = cv2.INTER_AREA) #Уменьшим картинку
	gray_image = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY) #Переведем в черно-белый формат

	#gray_image = cv2.medianBlur(gray_image, 3)
	#gray_image = cv2.GaussianBlur(gray_image,(3,3),0)

	avg=gray_image.mean() #Среднее значение пикселя
	ret, threshold_image = cv2.threshold(gray_image, avg, 255, 0) #Бинаризация по порогу
	#Рассчитаем хэш
	_hash2=""
	for x in range(8):
		for y in range(8):
			val=threshold_image[x,y]
			if val==255:
				_hash2=_hash2+"1"
			else:
				_hash2=_hash2+"0"

	l=len(_hash1)
	i=0
	count=0
	while i<l:
		if _hash1[i]!=_hash2[i]:
			count=count+1
		i=i+1

	if count > values[type_component]:
		return count, 'bad'	
	return count, 'good'
    

def pHash(component1, component2, type_component):
	values = {'805':13,'1206':15,'2010':14,'SOT23':19,'SWT-34A':12,'MINIMELF':13,'DO214AA':17,
			'TSSOP8':1300,'SOIC8':1300,'LQFP48':1300,'SOP4':1300,'LQH3N':1300,'POWERSO10':1300,'DO214AB':1300,
			'DO214AC':1300,'HC-49S/SMD':1300, 'SMD-C':1300, 'HC-49S/SMD':1300,'SO8':1300,'SO16':1300}
	
	(Image.fromarray(component1)).save('cashe/component1.jpg')
	(Image.fromarray(component2)).save('cashe/component2.jpg')

	
	#print(inspect.getmembers(imagehash, predicate=inspect.isfunction))

	hash1 = imagehash.dhash(Image.open('cashe/component1.jpg'))
	otherhash = imagehash.dhash(Image.open('cashe/component2.jpg'))

	
	result = hash1 - otherhash
	



	if result > values[type_component]:
		return result, 'bad'	
	return result, 'good'


