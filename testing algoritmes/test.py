import cv2
import matplotlib.pyplot as plt
from PIL import Image, ImageChops

import numpy as np


def hist(image_gaussian):
	histr = cv2.calcHist([image_gaussian],[0],None,[256],[0,256])
	plt.plot(histr)
	plt.show()

def showImage(image1, name = 'Image', size = .5, mouse = False):

	image1 = cv2.resize(image1, (0,0), fx=size, fy=size)
	image1 = cv2.cvtColor(image1, cv2.COLOR_BGR2RGB)
	cv2.imshow(name, image1)

	if mouse == True: 
		cv2.setMouseCallback(name, getCoordinats)

	cv2.waitKey()
	cv2.destroyAllWindows()

def changeImage(image1):



	image1 = cv2.cvtColor(image1, cv2.COLOR_BGR2GRAY) #Greylevel = 0.299R + 0.587G + 0.114B
	image1 = cv2.medianBlur(image1, 3)
	image1 = cv2.GaussianBlur(image1,(3,3),0)
	#ret, image1 = cv2.threshold(image1, 75, 255, 0)
	#image1 = cv2.adaptiveThreshold(image1,255,cv2.ADAPTIVE_THRESH_MEAN_C,\
            #cv2.THRESH_BINARY,11,2)

	return (image1)

def handCange(image):

	imageout = image.copy()

	image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	image = cv2.medianBlur(image, 3)
	image = cv2.GaussianBlur(image,(3,3),0)


	for x in range(len(image)):
		for y in range(len(image[x])):

			if image[x,y] > 0 and image[x,y] < 40:
				imageout[x,y] = (255,0,0)

	return (imageout)


imageOK = cv2.imread('OK.jpg')
imageOK2 = cv2.imread('OK2.jpg')
imageNOT = cv2.imread('NOT.jpg')


#imageOK = handCange(imageOK)

imageOK = changeImage(imageOK)
imageOK2 = changeImage(imageOK2)
imageNOT = changeImage(imageNOT)

#hist(image)


'''
showImage((imageOK), size=4)
showImage((imageNOT), size=4)
showImage((imageOK-imageNOT), size=4)
showImage((imageOK-imageOK2), size=4)


'''
i1 = cv2.absdiff(imageOK, imageNOT)
i2 = cv2.absdiff(imageOK, imageOK2)

ret1, i1 = cv2.threshold(i1, 150, 255, 0)
ret2, i2 = cv2.threshold(i2, 150, 255, 0)

showImage((i1), size=4)
showImage((i2), size=4)


showImage((cv2.absdiff(imageOK, imageNOT)), size = 4)

showImage((cv2.absdiff(imageOK, imageOK2)), size = 4)


